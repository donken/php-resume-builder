<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Author: David Onken

Contact information page and initial landing page.


Associated vars:
session_name
session_address
session_phone

-->

<?php

session_start(); // Start this session

// Check for incoming request variables and set the session variables
if (isset($_REQUEST['fullname']))
	$_SESSION['session_name'] = $_REQUEST['fullname'];

if (isset($_REQUEST['address']))
	$_SESSION['session_address'] = $_REQUEST['address'];

// In addition, parse if the subcategories are ints
if (isset($_REQUEST['phone1'],$_REQUEST['phone3'],$_REQUEST['phone3']))
	$_SESSION['session_phone'] = array('phone1' => $_REQUEST['phone1'],
			'phone2' => $_REQUEST['phone2'],
			'phone3' => $_REQUEST['phone3'],);

?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"
	type="text/javascript"></script>
<script src="validate.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="style.css" />
<title>Homework 5</title>
</head>

<body>

	<h2>Homework 5</h2>

	<?php
	if (isset($_SESSION['session_resume']))
		echo "<h3>Resume Name: ".$_SESSION['session_resume']."</h3>";
	require("helpers/variablechecker.php");
	require("helpers/loginbox.php");
	require("helpers/database.php");
	require("helpers/navpanel.php"); ?>

	<form method="post" onsubmit="return validateContactForm()">

		<div class=formentry>
	<h3>Contact Information</h3>

			<table class=information>
				<tr>
					<td><label for=full_name>Full Name:</label></td>
					<td><input type="text" id=full_name name="fullname" onchange="on_change()"
					<?php if ($_SESSION["session_name"] != null)
						echo 'value="'.$_SESSION["session_name"].'"'; ?> /></td>
				</tr>
				<tr>
					<td><label for=address>Address:</label></td>
					<td><input type="text" id=address name="address" onchange="on_change()"
					<?php // Check for address value
					if ($_SESSION["session_address"] != null)
						echo 'value="'.$_SESSION["session_address"].'"'; ?> /></td>
				</tr>
				<tr>
					<td><label for=phone>Phone Number:</label></td>
					<td id=phone><input type="text" size=3 maxlength=3 id=phone1
					<?php if ($_SESSION["session_phone"] != null) echo 'value="'.$_SESSION["session_phone"]['phone1'].'"'; ?>
						name="phone1" />-<input type="text" size=3 maxlength=3 id=phone2
						<?php if ($_SESSION["session_phone"] != null) echo 'value="'.$_SESSION["session_phone"]['phone2'].'"'; ?>
						name="phone2" />-<input type="text" size=4 maxlength=4 id=phone3
						<?php if ($_SESSION["session_phone"] != null) echo 'value="'.$_SESSION["session_phone"]['phone3'].'"'; ?>
						name="phone3" /></td>
			
			</table>

		</div>

		<p>
			<input type="submit" value="Submit" />
		</p>
		<p id=submittext>
		
		</p>

	</form>
	
	<?php require('helpers/footer.php'); ?>

</body>
</html>