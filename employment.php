<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Author: David Onken

Employment information page. Consists of a list of text areas that can be added and removed by Javascript

Associated vars:
session_employment
session_startdate
session_enddate

-->

<?php

session_start(); // Start this session

// Check for incoming request variables and set the session variables
if (isset($_REQUEST['employment']))
	$_SESSION['session_employment'] = $_REQUEST['employment'];

if (isset($_REQUEST['startdate']))
	$_SESSION['session_startdate'] = $_REQUEST['startdate'];

if (isset($_REQUEST['enddate']))
	$_SESSION['session_enddate'] = $_REQUEST['enddate'];


?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"
	type="text/javascript"></script>
<script src="validate.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="style.css" />
<title>Homework 5</title>
</head>

<body>

	<h2>Homework 5</h2>

	<?php
	if (isset($_SESSION['session_resume']))
		echo "<h3>Resume Name: ".$_SESSION['session_resume']."</h3>";
	require("helpers/variablechecker.php");
	require("helpers/loginbox.php");
	require("helpers/database.php");
	require("helpers/navpanel.php"); ?>

	<form method="post" onsubmit="return validateEmployForm()">

		<div class=formentry>
	<h3>Employment History</h3>
	<p>If you have no jobs, remove all rows (except the last one) and clear the first row entries.</p>

			<table class=information id=jobs>
				<tr>
					<th>Job Description</th>
					<th>Start Date (MM/DD/YYYY)</th>
					<th>End Date (MM/DD/YYYY)</th>
				</tr>
				<?php // If we have incoming session data, build it based on that, otherwise make a standard table 

if (isset($_SESSION['session_employment'], $_SESSION['session_startdate'], $_SESSION['session_enddate']))
	build_table();
else
	echo '<tr><td><textarea onchange="on_change()" id="employment1" name="employment[]" cols="20" rows="5"></textarea></td>
	<td><input type="text" onchange="on_change()" maxlength=10 class=center id=startdate1 name="startdate[]"/></td>
	<td><input type="text" onchange="on_change()" maxlength=10 class=center id=enddate1 name="enddate[]"/></td>
						<td><input type="button" class=remove id=removebutton1 value="Remove"/></td></tr>';?>

			</table>

			<p>
				<input type="button" id=addrow value="Add a Row" />
			</p>

		</div>

		<p>
			<input type="submit" value="Submit" />
		</p>
		<p id=submittext>
		
		</p>
		

	</form>
	
	<?php require('helpers/footer.php'); ?>

</body>

</html>
