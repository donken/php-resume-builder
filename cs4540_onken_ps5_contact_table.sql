CREATE DATABASE  IF NOT EXISTS `cs4540_onken` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `cs4540_onken`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: atr.eng.utah.edu    Database: cs4540_onken
-- ------------------------------------------------------
-- Server version	5.6.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ps5_contact_table`
--

DROP TABLE IF EXISTS `ps5_contact_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps5_contact_table` (
  `session_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `session_id_UNIQUE` (`session_id`),
  CONSTRAINT `FK11` FOREIGN KEY (`session_id`) REFERENCES `ps5_session_table` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Session ID, Name, Address, Phone';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps5_contact_table`
--

LOCK TABLES `ps5_contact_table` WRITE;
/*!40000 ALTER TABLE `ps5_contact_table` DISABLE KEYS */;
INSERT INTO `ps5_contact_table` VALUES (78,'Test','Test','111-111-1111'),(80,'Test2','Test2','111-111-1111'),(81,'Test','Test','111-111-1111'),(82,'aaa','a','103-130-1211'),(83,'Jack Johnson','123 Fake Street','801-555-1234'),(94,'Test Name','Test Address','400-500-6000');
/*!40000 ALTER TABLE `ps5_contact_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-03 18:35:05
