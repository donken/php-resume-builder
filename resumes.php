<?php
session_start();
require("helpers/functions.php");

if (!isset($_SESSION['login']))
{
	require('views/access.php');
	exit(0);
}

function parseSave($resume_name) {

	$contact = new contactInfo;

	$_SESSION['session_resume'] = $resume_name;

	// Parse the contact info
	if (isset($_SESSION['session_name']) && isset($_SESSION['session_address']) && isset($_SESSION['session_phone']))
	{
		$contact->setName($_SESSION['session_name']);
		$contact->setAddr($_SESSION['session_address']);
		$contact->setPhone($_SESSION['session_phone']);
	}
	else
	{
		$contact = '';
	}

	$job_desc = '';

	// Parse the job description
	if (isset($_SESSION['session_description']))
		$job_desc = $_SESSION['session_description'];

	$employment = '';

	if (isset($_SESSION['session_employment']) && isset($_SESSION['session_startdate']) && isset($_SESSION['session_enddate']))
	{
		$descrip = $_SESSION['session_employment'];
		$starts = $_SESSION['session_startdate'];
		$ends = $_SESSION['session_enddate'];
			
		$employment = array();
		// Foreach loop
		foreach($descrip as $a => $b)
		{
			$item = new employmentItem;
			$item->setDesc($descrip[$a]);
			$item->setStart($starts[$a]);
			$item->setEnd($ends[$a]);
			$employment[$a] = $item;
		}
	}

	//echo "$resume_name | ".$contact->getName()." | ".$job_desc." | ".$employment;

	return inputToDBH($resume_name, $contact, $job_desc, $employment, $_SESSION['login']);
}

function loadResume($resume_name, $user) {
	// Load all the variables from the resume object into the session
	
	// echo $resume_name.' / '.$user;

	if (!nameExists($resume_name, $user)) // If I got a junk name that doesn't exist, return false
		return false;

	// Build a resume object and start parsing out the data
	$resume = buildResume($resume_name, $user);
	$contact = $resume->getContact();
	$employment = $resume->getEmploy();

	// Get the name of this resume
	$_SESSION['session_resume'] = $resume->resume_name;

	if ($contact != '') // If there is contact data, save it
	{
		$_SESSION['session_name'] = $contact->getName();
		$_SESSION['session_address'] = $contact->getAddr();
		$_SESSION['session_phone'] = makePhone($contact->getPhone());
			
	}
	else
	{
		$_SESSION['session_name'] = null;
		$_SESSION['session_address'] = null;
		$_SESSION['session_phone'] = null;
	}

	if ($resume->getJob() != '') // If there is a job description, save it
		$_SESSION['session_description'] = $resume->getJob();
	else
		$_SESSION['session_description'] = null;

	if ($employment != '') // If there is employment data, save it
	{
			
		$descs = array();
		$starts = array();
		$ends = array();
			
		foreach ($employment as $key => $item) // Loop through each employment item and add it to the database
		{
			// Decompose the array of employment objects and make three arrays
			$descs[$key] = $item->getDesc();
			$starts[$key] = $item->getStart();
			$ends[$key] = $item->getEnd();
		}
			
		$_SESSION['session_employment'] = $descs;
		$_SESSION['session_startdate'] = $starts;
		$_SESSION['session_enddate'] = $ends;
	}
	else
	{
		$_SESSION['session_employment'] = null;
		$_SESSION['session_startdate'] = null;
		$_SESSION['session_enddate'] = null;
	}

	return true; // Return true to denote that it worked

}

$overwritealert = false;
$nameillegalalert = false;
$failedloadalert = false;
$faileddeletealert = false;
$nameillegalalert = false;

if (isset($_POST['submit']))
{
	// Check name legality
	$resume_name = trim($_POST['savename']);
	if ((strlen($resume_name) > 30 || strlen($resume_name) < 1) && $_POST['submit'] == 'Save my Resume')
		$nameillegalalert = true;
	else // Branch into four possibles if there is an incoming request
	{
		switch ($_POST['submit']) {
			case 'Save my Resume':
				if (nameExists($resume_name, $_SESSION['login']) && !($_POST['forceoverwrite'] == 'Force Overwrite'))
					$overridealert = true; // If the name exists and the force overwrite option is unchecked, send up an alert
				else
				{
					parseSave($resume_name); // Otherwise, save
				}
				break;
			case 'Preview':
				if (!isset($_POST['resumeitem']))
					$noselectedradiobutton = true;
				else
				{
					// Load variables
					$resume = buildResume($_POST['resumeitem'], $_SESSION['login']);
					$contact = $resume->getContact();

					$session_resume = $resume->resume_name;
					$session_name = '';
					$session_address = '';
					$session_phone = '';
					$session_description = '';

					if ($contact != '') {
						$session_name = $contact->getName();
						$session_address = $contact->getAddr();
						$session_phone = makePhone($contact->getPhone());
					}
					else {
						$session_name = "";
						$session_address = '';
						$session_phone = '';
					}
					$session_description = $resume->getJob();
					$employment = $resume->getEmploy();

					$session_employment = array();
					$session_startdate = array();
					$session_enddate = array();

					foreach ($employment as $key => $item) // Loop through each employment item and add it to the database
					{
						// Decompose the array of employment objects and make three arrays
						$session_employment[$key] = $item->getDesc();
						$session_startdate[$key] = $item->getStart();
						$session_enddate[$key] = $item->getEnd();
					}

					require("preview.php");
					exit();
				}
				break;
			case 'Load':
				if (!isset($_POST['resumeitem']))
					$noselectedradiobutton = true;
				else {
					if (loadResume($_POST['resumeitem'], $_SESSION['login'])) // Depending on what happens in the load process, set a flag
						$failedloadalert = false;
					else
						$failedloadalert = true;
				}
				break;
			case 'Delete':
				if (!isset($_POST['resumeitem']))
					$noselectedradiobutton = true;
				else {
					if (deleteFromDBH($_POST['resumeitem'], $_SESSION['login']))
					{
						if ($_POST['resumeitem'] == $_SESSION['session_resume'])
							$_SESSION['session_resume'] = null;
						$faileddeletealert = false;
					}
					else
						$faileddeletealert = true;
				}
				break;
		}

	}

}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--
Author: David Onken

Stored resumes access page. Delete, save, load, and preview resumes
-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"
	type="text/javascript"></script>
<script src="validate.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="style.css" />
<title>Homework 5</title>
</head>

<body>

	<h2>Homework 5</h2>

	<?php

	if (isset($_SESSION['session_resume']))
		echo "<h3>Resume Name: ".$_SESSION['session_resume']."</h3>";

	// Database input functions, the variable checker, and the nav panel
	require("helpers/variablechecker.php");
	require('helpers/loginbox.php');
	require("helpers/navpanel.php");
	?>

	<form method="post">
		<label id=savelabel for=savename>Please input a name for this resume
			(up to 30 characters)</label> <br /> <input type="text" id=save_name
			name="savename"
			value="<?php 
			if (isset($_POST['savename']))
				echo $_POST['savename']; ?>" /> <input type="checkbox"
			name="forceoverwrite" value="Force Overwrite">Force Overwrite <br />
		<input type="submit" name="submit" value="Save my Resume" /> <br />
		<?php 
		if (isset($overridealert) && $overridealert)
			echo '<p style="color:red">It looks like that name already exists, please check "Force Overwrite" and try again.</p>';
		if ($nameillegalalert)
		echo '<p style="color:red">Please make the name at least 1 character long and less than 30 characters.</p>'; ?>
		<br />
		<table class=information id=resumes>
			<tr>
				<th>Selection</th>
				<th>SID</th>
				<th>Resume Name</th>
			</tr>
			<?php 
			// Outputs the resumes as table rows
		buildResumesTable($_SESSION['login']); ?>
		</table>
		<label for=option>Action you wish to take on the selected resume:</label>
		<br /> <input type="submit" name="submit" value="Preview" /> <input
			type="submit" name="submit" value="Load" /> <input type="submit"
			name="submit" value="Delete" />
	</form>
	<?php 
	if (isset($failedloadalert) && $failedloadalert)
		echo '<p style="color:red">You attempted to (somehow) load an invalid name. Honestly, did you think POST spoofing would get you anywhere?</p>';
	if (isset($faileddeletedalert) && $faileddeletedalert)
		echo '<p style="color:red">You attempted to (somehow) delete an invalid name. Honestly, did you think POST spoofing would get you anywhere?</p>';
	if (isset($noselectedradiobutton) && $noselectedradiobutton)
		echo '<p style="color:red">There was no selected resume.</p>'; ?>

	<?php require("helpers/footer.php"); ?>

</body>

</html>
