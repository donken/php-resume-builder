<?php
session_start();

require("helpers/functions.php");
require("helpers/variablechecker.php");

// Check rights
if (!isset($_SESSION['login']) || getRights($_SESSION['login']) != "admin")
{
	require('views/access.php');
	exit(0);
}

$deleteflag = false;
$adminflag = false;
$userflag = false;

if (isset($_POST['submit']) && $_POST['submit'] == "Submit")
{
	foreach ($_POST['useroptions'] as $key => $option)
	{
		$uid = substr($option, 7);
		$actiontotake = substr($option, 0, 7);
		
		switch ($actiontotake) {
			case 'deleteu':
				if (!deleteUser($uid, $_SESSION['login']))
					$deleteflag = true;
				break;
			case 'toadmin':
				if (!makeAdmin($uid, $_SESSION['login']))
					$adminflag = true;
				break;
			case 'ctouser':
				if (!makeUser($uid, $_SESSION['login']))
					$userflag = true;
				break;
		}
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--
Author: David Onken

Admin page.
-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"
	type="text/javascript"></script>
<script src="validate.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="style.css" />
<title>Homework 5</title>
</head>

<body>

	<h2>Homework 5</h2>

	<?php
	// Login box and nav panel
	require('helpers/loginbox.php');
	require("helpers/navpanel.php");
	?>

	<form method="post">
		<table class=information id=users>
			<tr>
				<th>Username</th>
				<th>Real Name</th>
				<th>Privileges</th>
				<th>Action to Take</th>
			</tr>
			<?php 
			// Outputs the resumes as table rows
		buildUserTable($_SESSION['login']); ?>
		</table>
		<input type="submit" name="submit" value="Submit" />
	</form>
	<?php 
	if ($deleteflag)
		echo '<p style="color:red">Unable to delete at least one of those users. Ensure that they are not admins (and not yourself) and try again.</p>';
	if ($adminflag)
		echo '<p style="color:red">Unable to make at least one of those users an admin.</p>';
	if ($userflag)
		echo '<p style="color:red">Unable to make at least one of those admins a user. Ensure that there is at least one admin and try again.</p>'; ?>

	<?php require("helpers/footer.php"); ?>

</body>

</html>
