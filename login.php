<?php
session_start();

if (isset($_SESSION['login']))
{
	header('Location: '.$_SERVER['HTTP_REFERER']);
}

require("helpers/database.php");

$_SESSION['loginflag'] = false;

//check stuff
if (isset($_REQUEST['username']) && isset($_REQUEST['password']))
{
	if (userCheck($_REQUEST['username'], $_REQUEST['password']))
	{
		$_SESSION['login'] = $_REQUEST['username'];
	}
	else
	{
		$_SESSION['loginflag'] = true;
	}
}
header('Location: '.$_SERVER['HTTP_REFERER']);