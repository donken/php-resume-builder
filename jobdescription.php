<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Author: David Onken

Job description page. Consists of a single text area and a submission button.

Also the four navigation buttons are displayed.

Associated vars:
session_description

-->

<?php

session_start(); // Start this session

// Check for incoming request variables and set the session variables
if (isset($_REQUEST['description']))
	$_SESSION['session_description'] = $_REQUEST['description'];

?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"
	type="text/javascript"></script>
<script src="validate.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="style.css" />
<title>Homework 5</title>
</head>

<body>

	<h2>Homework 5</h2>

	<?php
	if (isset($_SESSION['session_resume']))
		echo "<h3>Resume Name: ".$_SESSION['session_resume']."</h3>";
	require("helpers/variablechecker.php");
	require("helpers/loginbox.php");
	require("helpers/database.php");
	require("helpers/navpanel.php"); ?>

	<form method="post" onsubmit="return validateJobForm()">

		<div class=formentry>
			<h3>Job Description</h3>

			<p id="job_description_label">
				Description of Job you are seeking:<br />
				<textarea id="job_description" onchange="on_change()"
					name="description" cols="50" rows="10"><?php if (isset($_SESSION['session_description']) && $_SESSION['session_description'] != "") echo $_SESSION['session_description']; ?></textarea>
			</p>
		</div>

		<p>
			<input type="submit" value="Submit" />
		</p>
		<p id=submittext></p>


	</form>

	<?php require('helpers/footer.php'); ?>

</body>

</html>
