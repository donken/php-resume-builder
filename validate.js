/**
 * Author: David Onken
 * 
 * Contains methods to validate the forms in each .php file and the table javascript support
 * 
 * validateContactForm()
 * validateJobForm()
 * validateEmployForm()
 * 
 */

var changed = false;


$(function() {
	window.onbeforeunload = closingWarning
	
	$("#jobs .remove").click(function() {
		remove_row(this);
	}); // Delete this row

	$("[id='addrow']").click(add_row);
	$("[id='help']").click(openwindow);
	
});

function on_change()
{
	changed = true;
}

function closingWarning(){
	if (changed)
		return "It looks like you've made changes on this page. If you do not submit before leaving, your entries will be lost.";
	else
		return;
}

/**
 * Validates the contact form type.
 * @returns {Boolean}
 */
function validateContactForm(){
	// Check the phone number slots. The address and name are open ended
	
	var p1 = document.getElementById('phone1').value;
	var p2 = document.getElementById('phone2').value;
	var p3 = document.getElementById('phone3').value;
	
	if (document.getElementById('full_name').value.trim() == "" ||
			document.getElementById('address').value.trim() == "")
		{
		var str = "There were some problems with the input form. Please revise the fields and resubmit.";
		document.getElementById('submittext').innerHTML = str.fontcolor("red");
		return false;
		}
	
	var pattern1 = /[0-9][0-9][0-9]/;
	var pattern2 = /[0-9][0-9][0-9][0-9]/;
	
	if (pattern1.test(p1) && pattern1.test(p2) && pattern2.test(p3))
		{
		var str = "Submitted successfully!";
		document.getElementById('submittext').innerHTML = str.fontcolor("green");
		changed = false;
		return true;
		}
	else
		{
		var str = "There were some problems with the input form. Please revise the fields and resubmit.";
		document.getElementById('submittext').innerHTML = str.fontcolor("red");
		return false;
		}
}

function validateJobForm(){
	var textbox = document.getElementById('job_description');
	var str = textbox.value.trim();
	
	if (str == "")
		{
		var str = "There were some problems with the input form. Please revise the fields and resubmit.";
		document.getElementById('submittext').innerHTML = str.fontcolor("red");
		return false;
		}
	else
		{
		var str = "Submitted successfully!";
		document.getElementById('submittext').innerHTML = str.fontcolor("green");
		changed = false;
		return true;
		}
}

/**
 * Validates the employ form type
 * @returns {Boolean}
 */
function validateEmployForm(){
	// Checking all the start/end date forms
	var table = document.getElementById('jobs');
	var len = table.rows.length - 1;
	
	// From stack overflow: http://stackoverflow.com/questions/6402743/regular-expression-for-mm-dd-yyy-in-javascript
	var regex = new RegExp("^((0?[1-9]|1[012])[- \/.](0?[1-9]|[12][0-9]|3[01])[- \/.](19|20)?[0-9]{2})*$");
	
	var flag = true;
	
	for (var x = 1; x <= len; x++)
		{
		var start = document.getElementById("startdate" + x).value;
		var end = document.getElementById("enddate" + x).value;
		var textbox = document.getElementById("employment" + x);
		var str = textbox.value.trim();
		
		if (start == "" && end == "" && str == "" && len == 1)
			return true;
		else if (start == "" || end == "" || str == "")
			flag = false;
			
		
		if (!(regex.test(start) && regex.test(end)))
			flag = false;
		}
	
	if (flag == false)
		{
		var str = "There were some problems with the input form. Please revise the fields and resubmit.";
		document.getElementById('submittext').innerHTML = str.fontcolor("red");
		}
	else
		{
		var str = "Submitted successfully!";
		document.getElementById('submittext').innerHTML = str.fontcolor("green");
		changed = false;
		}
	return flag;
	
}

/**
 * Validates all the types of forms.
 * @returns {Boolean}
 */
function validateAllForm()
{
	if (validateContactForm() && validateJobForm() && validateEmployForm())
		{
		return true;
		}
	else
		{
		var str = "There were some problems with the input form. Please revise the fields and resubmit.";
		document.getElementById('submittext').innerHTML = str.fontcolor("red");
		return false;
		}
}

function validateUser() {
	var textbox = document.getElementById('username');
	var str = textbox.value.trim();
	
	if (str == "")
		{
		var str = "<p>Your username must consist of characters.</p>";
		document.getElementById('errors').innerHTML = str.fontcolor("red");
		return false;
		}
	return true;
}

function validateReal() {
	var textbox = document.getElementById('realname');
	var str = textbox.value.trim();
	
	if (str == "")
		{
		var str = "<p>Your real name must consist of characters.</p>";
		document.getElementById('errors').innerHTML = str.fontcolor("red");
		return false;
		}
	return true;
}

function validatePasswords() {
	var pass = document.getElementById('password');
	var pass2 = document.getElementById('password2');
	var str = pass.value.trim();
	var str2 = pass2.value.trim();

	if (str == "")
		{
		var str = "<p>Your password must consist of characters.</p>";
		document.getElementById('errors').innerHTML = str.fontcolor("red");
		return false;
		}
	if (str != str2)
		{
		var str = "<p>Your passwords do no match.</p>";
		document.getElementById('errors').innerHTML = str.fontcolor("red");
		return false;
		}
	
	return true;
	
}

function validateRegisterForm() {
	return validatePasswords() && validateReal() && validateUser();
}

/**
 * Removes the row that corresponds to the button pressed
 * @param remove_button
 */
function remove_row(remove_button)
{
	var table = document.getElementById('jobs');
	var len = table.rows.length; // Get length and exit if it's under 2
	if (len <= 2)
		return;
	
	// Otherwise delete the row
	var col = remove_button.parentNode;
	var row = col.parentNode;
	table.deleteRow(row.rowIndex);
	
	return;

}

/**
 * Adds a row at the bottom of the jobs table.
 */
function add_row()
{
	var table = document.getElementById('jobs');
	
	var rowCount = table.rows.length; // Get the # of rows and insert it at the end (rowCount)
    var row = table.insertRow(rowCount);

    // Build a text area element
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("textarea");
    element1.id = "employment" + (rowCount);
    element1.name="employment[]";
    element1.cols = 20;
    element1.rows = 5;
    cell1.appendChild(element1);

    // Build a start date element
    var cell2 = row.insertCell(1);
    element2 = document.createElement("input");
    element2.type = "text";
    element2.id = "startdate" + (rowCount);
    element2.name = "startdate[]";
    cell2.appendChild(element2);

    // Build a end date element
    var cell3 = row.insertCell(2);
    var element3 = document.createElement("input");
    element3.type = "text";
    element3.id = "enddate" + (rowCount);
    element3.name = "enddate[]";
    cell3.appendChild(element3);
    
    // Build a remove button and make it execute when pressed
    var cell4 = row.insertCell(3);
    var element4 = document.createElement("input");
    element4.type = "button";
    element4.id = "removebutton" + (rowCount);
    element4.value = "Remove";
    element4.onclick = function() {
    	remove_row(element4);
    };
    cell4.appendChild(element4);
    
    // The remove button needs a remove class assigned slightly differently
    var button = document.getElementById('removebutton' + rowCount);
    $("[id='" + ("removebutton" + rowCount) + "']").addClass("remove");

	
}

function openwindow()
{
	window.open("help.html","_blank","height=500,width=600, status=yes,toolbar=no,menubar=no,location=no");
}