<!-- 
Author: David Onken

Useful abstraction. Contains a simple footer that the files use. -->

<hr/>

<footer>

<p>Created by David Onken - u0760062</p>
<p>Contact information: <a href="mailto:David.Onken@utah.edu">David.Onken@utah.edu</a>.</p>
<input type=button id=help value="Help!">

</footer>