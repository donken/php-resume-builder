<?php

if (!isset($_SESSION['loginflag']))
{
	$_SESSION['loginflag'] = false;
}

if (isset($_SESSION['login']))
{
	// If the login is set
	echo '<div class="login">Signed in as: '.$_SESSION['login'].'<br/><form method="post" action="logout.php"><input type="submit" value="Logout" /></form></div>';
}
else
{
	// Otherwise put in the login box
	echo '<div class="login"><form method="post" action="login.php"><table class=information>
				<tr>
					<td><label for=username>Username:</label></td>
					<td><input type="text" id=username name="username" /></td>
				</tr>
				<tr>
					<td><label for=password>Password:</label></td>
					<td><input type="password" id=password name="password" /></td>
				</tr>
	</table><input type="submit" value ="Login" />
	<a href="register.php">Register Now</a></form>';
	if ($_SESSION['loginflag'])
	{
		echo '<p style="red">There was a problem with your login information. Try again.</p>';
		$_SESSION['loginflag'] = false;
	}
	echo '</div>';
}