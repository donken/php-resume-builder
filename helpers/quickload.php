<?php
require("helpers/variablechecker.php");
require("helpers/functions.php");

/**
 * Generates li entries based on the session variables
 */
function generateitems() {
	global $session_employment;
	global $session_startdate;
	global $session_enddate;

	$descrip = $session_employment;
	$starts = $session_startdate;
	$ends = $session_enddate;
	
	if ($descrip = '')
	{
		echo '<p>No Work Experience</p>';
		return;
	}

	// Build the items otherwise
	echo '<ul>';
	foreach($descrip as $a => $b)
	{
		echo '<li class=resumetext>From '.$starts[$a].' to '.$ends[$a].':<br/><br/>' .$descrip[$a].'</li><br/>';
	}
	echo '</ul>';

}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Author: David Onken

Preview page to review data.

-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"
	type="text/javascript"></script>
<script src="validate.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="style.css" />
<title>Preview</title>
</head>

<body>


	<?php
	// Load variables
	$resume = buildResume($name, $login);
	$contact = $resume->getContact();
	
	$session_resume = $resume->resume_name;
	$session_name = '';
	$session_address = '';
	$session_phone = '';
	$session_description = '';
	
	if ($contact != '') {
		$session_name = $contact->getName();
		$session_address = $contact->getAddr();
		$session_phone = makePhone($contact->getPhone());
	}
	else {
		$session_name = "";
		$session_address = '';
		$session_phone = '';
	}
	
	$session_description = $resume->getJob();
	$employment = $resume->getEmploy();

	$session_employment = array();
	$session_startdate = array();
	$session_enddate = array();

	foreach ($employment as $key => $item) // Loop through each employment item and add it to the database
	{
		// Decompose the array of employment objects and make three arrays
		$session_employment[$key] = $item->getDesc();
		$session_startdate[$key] = $item->getStart();
		$session_enddate[$key] = $item->getEnd();
	}


	if (isset($session_resume))
		echo "<h2>".$session_resume." Preview</h2>"; ?>

	<p id=contactinfo>
		<?php 
		if ($session_name != '')
			echo $session_name.'<br/>';
		else
			echo "<p class=notfound>No Data Input for Name</p>";

		if ($session_address != '')
			echo $session_address.'<br/>';
		else
			echo "<p class=notfound>No Data Input for Address</p>";

		if ($session_phone != '')
			echo $session_phone["phone1"].'-'.$session_phone["phone2"].'-'.$session_phone["phone3"];
		else
			echo "<p class=notfound>No Data Input for Phone</p>";
		?>
	</p>

	<hr />

	<p class=resumeheader>Jobs that I'm Looking For:</p>
	<p class=resumetext>
		<?php 
		if ($session_description != '')
			echo $session_description;
		else
		echo "<p class=notfound>No Data Input for Job Sought Description</p>"; ?>
	</p>
	<hr />

	<p class=resumeheader>Employment Experience:</p>
	<?php generateitems();

	require('helpers/footer.php'); ?>

</body>

</html>

