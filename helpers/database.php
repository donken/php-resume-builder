<?php

/*
 * Author: David Onken
 *
 * Database Access file. Contains all the necessary functions for database access.
 * 
 */

require("authentication.php");
require("classes.php");

/**
 * Opens a database connection.
 * 
 * @return PDO
 */
function openDBConnection () {
	
	global $dbpassword;
	$DBH = new PDO("mysql:host=$hostname;dbname=cs4540_onken",
			'cs4540_onken', $dbpassword);
	$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $DBH;
}

/**
 * Gets this user's resumes
 * 
 * @param string $user
 * @return array
 */
function findResumes($user) {
	try {
		$DBH = openDBConnection();
			
		$stmt = $DBH->prepare("SELECT session_id,session_name FROM cs4540_onken.ps5_session_table  WHERE uid=?;");
		$stmt->bindValue(1, getUID($user));
		$stmt->execute();
		$resume_details = array();
		while ($row = $stmt->fetch()) {
			$resume_details[$row['session_name']] = $row['session_id']; // Associate the session id with the name
		}
		$DBH = null;
		return $resume_details;
	}
	catch (PDOException $e) {
		reportDBError($e);
	}
}

/**
 * Originally returned all resumes for admins. Now it is just a driver for findResumes since I
 * realized that admins shouldn't be able to see everyone's resumes.
 * 
 * @param string $user
 * @return array
 */
function getResumes($user) {
	
	/*if (getRights($user) != "admin")
	{*/
		return findResumes($user);
	/*}
	else
	{
		try {
			$DBH = openDBConnection();
			
			$stmt = $DBH->prepare("SELECT session_name,session_id FROM cs4540_onken.ps5_session_table");
			$stmt->execute();
			$resume_details = array();
			while ($row = $stmt->fetch()) {
				$resume_details[$row['session_name']] = $row['session_id']; // Associate the session id with the name
			}
			$DBH = null;
			return $resume_details;
		}
		catch (PDOException $e) {
			reportDBError($e);
		}
	}*/
}

/**
 * Gets the list of users. Throws exception if not an admin (never happens).
 * 
 * @param string $user
 * @throws Exception
 * @return array
 */
function getUsers($user) {
	if (getRights($user) != "admin")
	{
		throw new Exception("Not an admin");
	}
	else
	{
		try {
			$DBH = openDBConnection();
			$stmt = $DBH->prepare("SELECT * FROM cs4540_onken.ps5_users_table;");
			$stmt->execute();
			$users = array();
			while ($row = $stmt->fetch()) {
				$useritem = new user;
				$useritem->username = $row['username'];
				$useritem->realname = $row['real_name'];
				$useritem->priv = $row['priv'];
				$users[$row['uid']] = $useritem; // Associate the object with the uid
			}
			$DBH = null;
			return $users;
		}
		catch (PDOException $e) {
			reportDBError($e);
		}
	}
}

/**
 * Checks if this resume name already exists for this user.
 * 
 * @param string $in
 * @param string $user
 */
function nameExists($in, $user) {
	
	$resume_name = trim($in);
	
	try {
		$DBH = openDBConnection();
		$stmt = $DBH->prepare("SELECT session_id FROM cs4540_onken.ps5_session_table WHERE session_name=? AND uid=?");
		$stmt->bindValue(1, $resume_name);
		$stmt->bindValue(2, getUID($user));
		$stmt->execute();
		$DBH = null;
		while ($row = $stmt->fetch()) {
			return true; // If it found something, then it clearly exists
		}
		return false; // Otherwise, it was empty, so it doesn't exist
	}
	catch (PDOException $e) {
		reportDBError($e);
	}
}

/**
 * Checks if this user exists.
 * 
 * @param string $user
 */
function userExists($user) {
	try {
		$DBH = openDBConnection();
		$stmt = $DBH->prepare("SELECT uid FROM cs4540_onken.ps5_users_table WHERE username=?;");
		$stmt->bindValue(1, $user);
		$stmt->execute();
		$DBH = null;
		while ($row = $stmt->fetch()) {
			return true; // If it found something, then it clearly exists
		}
		return false; // Otherwise, it was empty, so it doesn't exist
	}
	catch (PDOException $e) {
		reportDBError($e);
	}
}

/**
 * Returns a true or false based on if this user (with this password) exists
 * 
 * @param string $user
 * @param string $password
 * @return boolean
 */
function userCheck($user, $password) {
	try {
		
		$DBH = openDBConnection();
		$stmt = $DBH->prepare("SELECT uid,password FROM cs4540_onken.ps5_users_table WHERE username=?");
		$stmt->bindValue(1, $user);
		$stmt->execute();
		
		if ($row = $stmt->fetch()) {
			// Validate the password
			$hashedPassword = $row['password'];
			//echo $password;
			//echo $hashedPassword;
			echo computeHash($password, $hashedPassword);
			if (computeHash($password, $hashedPassword) == $hashedPassword) {
				return true;
			}
		}
		$DBH = null;
		
		return false; // Otherwise, it was empty, so it doesn't exist
	}
	catch (PDOException $e) {
		reportDBError($e);
	}
}

/**
 * Return the rights for this user.
 * 
 * @param string $user
 * @return string
 */
function getRights($user)
{
	
	try {
		$DBH = openDBConnection();
		$stmt = $DBH->prepare("SELECT priv FROM cs4540_onken.ps5_users_table WHERE username=?;");
		$stmt->bindValue(1, $user);
		$stmt->execute();
		$DBH = null;
		$row = $stmt->fetch();
		return $row['priv'];
	}
	catch (PDOException $e) {
		reportDBError($e);
	}
}

/**
 * Gets the rights of this UID
 * 
 * @param string $uid
 * @return string
 */
function getRightsUID($uid) {
	try {
		$DBH = openDBConnection();
		$stmt = $DBH->prepare("SELECT priv FROM cs4540_onken.ps5_users_table WHERE uid=?;");
		$stmt->bindValue(1, $uid);
		$stmt->execute();
		$DBH = null;
		$row = $stmt->fetch();
		return $row['priv'];
	}
	catch (PDOException $e) {
		reportDBError($e);
	}
}

/**
 * Gets the UID of this user.
 * 
 * @param string $user
 */
function getUID($user) {
	try {
		$DBH = openDBConnection();
		$stmt = $DBH->prepare("SELECT uid FROM cs4540_onken.ps5_users_table WHERE username=?;");
		$stmt->bindValue(1, $user);
		$stmt->execute();
		$DBH = null;
		$row = $stmt->fetch();
		return $row['uid'];
	}
	catch (PDOException $e) {
		reportDBError($e);
	}
}

/**
 * Check if they have the rights to edit this resume.
 * 
 * @param string $in
 * @param string $user
 * @return boolean
 */
function rightsCheck($in, $user){
	$resume_name = trim($in);
	try {
		$DBH = openDBConnection();
		$stmt = $DBH->prepare("SELECT uid FROM cs4540_onken.ps5_session_table WHERE session_name=? AND uid=?;");
		$stmt->bindValue(1, $resume_name);
		$stmt->bindValue(2, getUID($user));
		$stmt->execute();
		$DBH = null;
		$row = $stmt->fetch();
		if (getUID($user) == $row['uid'])
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	catch (PDOException $e) {
		reportDBError($e);
	}
}

/**
 * Deletes the resume from the database.
 * 
 * @param string $in
 * @param string $user
 * @return boolean
 */
function deleteFromDBH($in, $user) {
	$resume_name = trim($in);
	if ($resume_name == "")
		return false;
	
	if (!rightsCheck($in, $user))
		return false;
	
	try {
	$DBH = openDBConnection();
	$DBH->beginTransaction();
	
	$stmt = $DBH->prepare("DELETE FROM `cs4540_onken`.`ps5_session_table` WHERE `session_name`=? AND uid=?");
	$stmt->bindValue(1, $resume_name);
	$stmt->bindValue(2, getUID($user));
	$stmt->execute();
	
	$DBH->commit(); // Commit the changes
	$DBH = null;
	return true;
	}
	catch (PDOException $e) {
		if ($e->getCode() == 23000) {
			return false;
		}
		reportDBError($e);
	}
}

/**
 * Delete the uid (and by extension all related data) from the database.
 * 
 * @param string $uid
 * @param string $user
 */
function deleteUser($uid, $user) {
	if (getRights($user) != 'admin')
		return false;
	
	if (getRightsUID($uid) == 'admin')
		return false;
	
	try {
		$DBH = openDBConnection();
		$DBH->beginTransaction();
	
		$stmt = $DBH->prepare("DELETE FROM `cs4540_onken`.`ps5_users_table` WHERE `uid`=?;");
		$stmt->bindValue(1, $uid);
		$stmt->execute();
	
		$DBH->commit(); // Commit the changes
		$DBH = null;
		return true;
	}
	catch (PDOException $e) {
		if ($e->getCode() == 23000) {
			return false;
		}
		reportDBError($e);
	}
}

/**
 * Checks if there are more than one admins still in the database.
 * @return boolean
 */
function moreAdmins()
{
	try {
		$DBH = openDBConnection();
		$stmt = $DBH->prepare("SELECT * FROM cs4540_onken.ps5_users_table WHERE priv='admin';");
		$stmt->execute();
		$DBH = null;
		$count = 0;
		while ($row = $stmt->fetch()) {
			$count++;
		}
		return ($count > 1); // If $count is greater than 1, there are more
	}
	catch (PDOException $e) {
		reportDBError($e);
	}
	
}

/**
 * Make this uid an admin.
 * 
 * @param string $uid
 * @param string $user
 * @return boolean
 */
function makeAdmin($uid, $user) {
	if (getRights($user) != 'admin')
		return false;
	
	try {
		$DBH = openDBConnection();
		$DBH->beginTransaction();
	
		$stmt = $DBH->prepare("UPDATE `cs4540_onken`.`ps5_users_table` SET `priv`='admin' WHERE `uid`=?;");
		$stmt->bindValue(1, $uid);
		$stmt->execute();
	
		$DBH->commit(); // Commit the changes
		$DBH = null;
		return true;
	}
	catch (PDOException $e) {
		if ($e->getCode() == 23000) {
			return false;
		}
		reportDBError($e);
	}
}

/**
 * Make this uid a user IFF there is more than one admin.
 * 
 * @param string $uid
 * @param string $user
 */
function makeUser($uid, $user) {
	if (getRights($user) != 'admin')
		return false;
	
	// Check if there is still an admin
	if (!moreAdmins())
		return false;
	
	try {
		$DBH = openDBConnection();
		$DBH->beginTransaction();

		$stmt = $DBH->prepare("UPDATE `cs4540_onken`.`ps5_users_table` SET `priv`='user' WHERE `uid`=?;");
		$stmt->bindValue(1, $uid);
		$stmt->execute();

		$DBH->commit(); // Commit the changes
		$DBH = null;
		return true;
	}
	catch (PDOException $e) {
		if ($e->getCode() == 23000) {
			return false;
		}
		reportDBError($e);
	}
}

/**
 * Input this user to the database.
 * 
 * @param string $user
 * @param string $real
 * @param string $password
 * @return boolean
 */
function inputUser($user, $real, $password){
	if (userExists($user))
		return false;
	
	//echo "input ".$user.' | '.$real.' | '.$password;
	
	try {
		$DBH = openDBConnection();
		$DBH->beginTransaction();
	
		$stmt = $DBH->prepare("INSERT INTO `cs4540_onken`.`ps5_users_table` (`username`, `priv`, `password`, `real_name`) VALUES (?, 'user', ?, ?);");
		$stmt->bindValue(1, $user);
		$hashedPassword = computeHash($password, makeSalt());
		$stmt->bindValue(2, $hashedPassword);
		$stmt->bindValue(3, $real);
		$stmt->execute();
	
		$DBH->commit(); // Commit the changes
		$DBH = null;
		return true;
	}
	catch (PDOException $e) {
		if ($e->getCode() == 23000) {
			return false;
		}
		reportDBError($e);
	}
}

/**
 * Combined function to insert all relevant information to the database.
 * 
 * @param string $in
 * @param object $contact_info
 * @param string $job_desc
 * @param array $employment
 * @param string $user
 * @return boolean
 */
function inputToDBH($in, $contact_info, $job_desc, $employment, $user) {
	$resume_name = trim($in);
	if ($resume_name == "")
		return false;
	
	try {
		$DBH = openDBConnection();
		$DBH->beginTransaction();
		
		if (nameExists($resume_name, $user)) // If it exists, destroy it
			deleteFromDBH($resume_name, $user); // Delete the resume name from the database
		
		$uid = getUID($user);
		
		$stmt = $DBH->prepare('INSERT INTO `cs4540_onken`.`ps5_session_table` (`session_name`, `uid`) VALUES (?, ?)');
		$stmt->bindValue(1, $resume_name);
		$stmt->bindValue(2, $uid);
		$stmt->execute();
		
		
		// Get the session_id
		$stmt = $DBH->prepare('SELECT session_id FROM cs4540_onken.ps5_session_table WHERE session_name=? AND uid=?');
		$stmt->bindValue(1, $resume_name);
		$stmt->bindValue(2, $uid);
		$stmt->execute();
		$row = $stmt->fetch();
		$session_id = $row['session_id'];
		
		/*$stmt = $DBH->prepare("UPDATE `cs4540_onken`.`ps5_userresumes_table` SET `session_id`=? WHERE `session_id`='0';");
		$stmt->bindValue(1, $session_id);
		$stmt->execute();*/
		
		
		// Add to the userresumes table
		/*$stmt = $DBH->prepare('INSERT INTO `cs4540_onken`.`ps5_userresumes_table` (`uid`, `session_id`) VALUES (?, ?);');
		$stmt->bindValue(1, $uid);
		$stmt->bindValue(2, $session_id);
		$stmt->execute();*/
	
		if ($contact_info != '') // If there is contact info, input it
		{
			$contactphone = $contact_info->getPhone();
			$phone = $contactphone['phone1'].'-'.$contactphone['phone2'].'-'.$contactphone['phone3'];
			
			$stmt = $DBH->prepare("INSERT INTO `cs4540_onken`.ps5_contact_table (`session_id`, `name`, `address`, `phone`) VALUES (?, ?, ?, ?)");
			$stmt->bindValue(1, $session_id);
			$stmt->bindValue(2, $contact_info->getName());
			$stmt->bindValue(3, $contact_info->getAddr());
			$stmt->bindValue(4, $phone);
			$stmt->execute();
		}
	
		if ($job_desc != '') // If there is a job description, input it
		{
			$stmt = $DBH->prepare("INSERT INTO `cs4540_onken`.ps5_job_table (`session_id`, `description`) VALUES (?, ?)");
			$stmt->bindValue(1, $session_id);
			$stmt->bindValue(2, $job_desc);
			$stmt->execute();
		}
		
		if ($employment != '') // If there is employment data, input it
		{
			foreach ($employment as $key => $item) // Loop through each employment item and add it to the database
			{
				$stmt = $DBH->prepare("INSERT INTO `cs4540_onken`.ps5_employ_table (`session_id`, `description`, `startdate`, `enddate`) VALUES (?, ?, ?, ?)");
				$stmt->bindValue(1, $session_id);
				$stmt->bindValue(2, $item->getDesc());
				$stmt->bindValue(3, $item->getStart());
				$stmt->bindValue(4, $item->getEnd());
				$stmt->execute();
			}
		}
		$DBH->commit(); // Commit the changes
		$DBH = null;
		return true;
	}
	catch (PDOException $e) {
		if ($e->getCode() == 23000) {
			return false;
		}
		reportDBError($e);
	}
}

/**
 * Function to pull contact data for the user and resume in question.
 * 
 * @param string $resume_name
 * @param string $user
 * @return string|contactInfo
 */
function getContactDBH($resume_name, $user){
	
	if (!rightsCheck($resume_name, $user))
		return '';
	
	try {
		$DBH = openDBConnection();
		$stmt = $DBH->prepare("SELECT name,address,phone FROM cs4540_onken.ps5_session_table NATURAL JOIN cs4540_onken.ps5_contact_table where session_name=? AND uid=?");
		$stmt->bindValue(1, $resume_name);
		$stmt->bindValue(2, getUID($user));
		$stmt->execute(); // Execute the find for name, address, phone
		
		$contact_info = new contactInfo;
		if($row = $stmt->fetch()) {
			$contact_info->setName($row['name']);
			$contact_info->setAddr($row['address']);
			$contact_info->setPhone($row['phone']);
		}
		else
			return '';
		
		$DBH = null;
		
		return $contact_info;
	}
	catch (PDOException $e) {
		reportDBError($e);
	}
}

// Function to pull job data
function getJobDBH($resume_name, $user){
	if (!rightsCheck($resume_name, $user))
		return null;
	try {
		$DBH = openDBConnection();
		$stmt = $DBH->prepare("SELECT description FROM cs4540_onken.ps5_session_table NATURAL JOIN cs4540_onken.ps5_job_table where session_name=? AND uid=?");
		$stmt->bindValue(1, $resume_name);
		$stmt->bindValue(2, getUID($user));
		$stmt->execute();

		if($row = $stmt->fetch())
			$job_info = $row['description'];
		else
			return '';
		
		$DBH = null;

		return $job_info;
	}
	catch (PDOException $e) {
		reportDBError($e);
	}
}

/**
 * Function to pull employment data for the resume and user in question.
 * 
 * @param string $resume_name
 * @param string $user
 * @return string|multitype:employmentItem
 */
function getEmployDBH($resume_name, $user){
	if (!rightsCheck($resume_name, $user))
		return '';
	try {
		$DBH = openDBConnection();
		$stmt = $DBH->prepare("SELECT employ_id,description,startdate,enddate FROM cs4540_onken.ps5_session_table NATURAL JOIN cs4540_onken.ps5_employ_table where session_name=? AND uid=?");
		$stmt->bindValue(1, $resume_name);
		$stmt->bindValue(2, getUID($user));
		$stmt->execute();
		
		$employment = array();
		while ($row = $stmt->fetch()) {
			$newItem = new employmentItem;
			
			$newItem->setDesc($row['description']);
			$newItem->setStart($row['startdate']);
			$newItem->setEnd($row['enddate']);
			$employment[$row['employ_id']] = $newItem;
		}
		$DBH = null;

		return $employment;
	}
	catch (PDOException $e) {
		reportDBError($e);
	}
}
	
function reportDBError ($exception) {
	$file = fopen("log.txt", "a");
	fwrite($file, date(DATE_RSS));
	fwrite($file, "\n");
	fwrite($file, $exception->getMessage());
	fwrite($file, $exception->getTraceAsString());
	fwrite($file, "\n");
	fwrite($file, "\n");
	fclose($file);
	require("views/error.php");
	exit();
}