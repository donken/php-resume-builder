<?php
/*
 * Classes for ease of use
 */

class user {
	public $username;
	public $realname;
	public $priv;
}

class resume {
	public $resume_name;
	private $contact_info;
	private $job_desc;
	private $employment;
	
	public function setContact($contact) {
		$this->contact_info = $contact;
	}
	
	public function setJob($desc) {
		$this->job_desc = $desc;
	}
	
	public function setEmploy($employ) {
		$this->employment = $employ;
	}
	
	public function getContact() {
		return $this->contact_info;
	}
	
	public function getJob() {
		return $this->job_desc;
	}
	
	public function getEmploy() {
		if (count($this->employment) == 0)
			return '';
		else
			return $this->employment;
	}
}

class contactInfo {
	private $name;
	private $address;
	private $phone;
	
	public function setName($newName) {
		$this->name = $newName;
	}
	
	public function setAddr($newAddr) {
		$this->address = $newAddr;
	}
	
	public function setPhone($newPhone) {
		$this->phone = $newPhone;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function getAddr() {
		return $this->address;
	}
	
	public function getPhone() {
		return $this->phone;
	}
}

class employmentItem {
	private $description;
	private $startdate;
	private $enddate;

	public function setDesc($newDesc) {
		$this->description = $newDesc;
	}

	public function setStart($newStart) {
		$this->startdate = $newStart;
	}

	public function setEnd($newEnd) {
		$this->enddate = $newEnd;
	}

	public function getDesc() {
		return $this->description;
	}

	public function getStart() {
		return $this->startdate;
	}

	public function getEnd() {
		return $this->enddate;
	}
}