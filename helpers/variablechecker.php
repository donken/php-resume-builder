<!--

Author: David Onken

Contains the code for useful validity checking functions.


-->

<?php 

if (!isset($_SESSION['session_phone']))
	$_SESSION['session_phone'] = "";

if (!isset($_SESSION['session_name']))
	$_SESSION['session_name'] = "";

if (!isset($_SESSION['session_employment']))
	$_SESSION['session_employment'] = Array();

if (!isset($_SESSION['session_startdate']))
	$_SESSION['session_startdate'] = "";

if (!isset($_SESSION['session_enddate']))
	$_SESSION['session_enddate'] = "";

if (!isset($_SESSION['session_startdate']))
	$_SESSION['session_startdate'] = "";

if (!isset($_SESSION['session_address']))
	$_SESSION['session_address'] = "";

// Functions for checking the validity of session variables
function contactcheck(){

	$flag = true;
	
	$phone = $_SESSION['session_phone'];
	
	if (isset($_SESSION['session_name']))
	{
		// Check for valid name
		if (trim($_SESSION['session_name']) == "")
			$flag = false;
	} else
		$flag = false;
	
	if (isset($_SESSION['session_address']))
	{
		if (trim($_SESSION['session_address']) == "")
			$flag = false;
	} else
		$flag = false;
	
	// Check phone array for validity
	if (isset($phone) && $phone != "")
	{
		$pattern1 = '/[0-9][0-9][0-9]/';
		$pattern2 = '/[0-9][0-9][0-9][0-9]/';
		if (!(preg_match($pattern1,$phone['phone1']) && preg_match($pattern1,$phone['phone2']) && preg_match($pattern2,$phone['phone3'])))
		{
			$flag = false;
		}
			
	} else
		$flag = false;

	return $flag;
}

function descriptioncheck() {
	if (!isset($_SESSION['session_description']) || trim($_SESSION['session_description']) == "")
		return false;
	else
		return true;
}

function employmentcheck() {
// Check the description for characters and the month format

	$descrip = $_SESSION['session_employment'];
	$starts = $_SESSION['session_startdate'];
	$ends = $_SESSION['session_enddate'];
	
	$flag = true;
	$pattern = '~^(\d\d)/(\d\d)/(\d\d\d\d)$~';
	
	// Foreach loop
	foreach($descrip as $a => $b)
	{
		if (count($descrip) == 1 && trim($descrip[$a]) == "" && trim($starts[$a]) == "" && trim($ends[$a]) == "")
			return true;
		else
			if (trim($descrip[$a]) == "" && !preg_match($pattern,$starts[$a]) && !preg_match($pattern,$ends[$a]))
				$flag = false;
		
	}
	
	return $flag;
	
}

/**
 * Check all the session variables
 * @return boolean
 */
function checkall() {
	return contactcheck() && descriptioncheck() && employmentcheck();
}

function build_table()
{
	$descrip = $_SESSION['session_employment'];
	$starts = $_SESSION['session_startdate'];
	$ends = $_SESSION['session_enddate'];

	$index = 1;

	// Foreach loop
	foreach($descrip as $a => $b)
	{
		echo '<tr><td><textarea onchange="on_change()" id="employment'.$index.'" name="employment[]" cols="20" rows="5">'.$descrip[$a].'</textarea></td>
		<td><input type="text" onchange="on_change()" maxlength=10 class=center id=startdate'.$index.' name="startdate[]" value="'.$starts[$a].'"/></td>
		<td><input type="text" onchange="on_change()" maxlength=10 class=center id=enddate'.$index.' value="'.$ends[$a].'" name="enddate[]"/></td>
		<td><input type="button" class=remove id=removebutton'.$index.' value="Remove"/></td></tr>';
		$index++;
	}

}

$checkimg = '<img src="./images/check.gif" alt="Check"/> No problems found';
$failimg = '<img src="./images/fail.gif" alt="Fail"/> Problem detected';

$checkimgf = '<img src="./images/check.gif" alt="Check"/> Ready';
$failimgf = '<img src="./images/fail.gif" alt="Fail"/> Not Ready';

$folderimg = '<img src="./images/folder.gif" alt="Load/Save"/>';