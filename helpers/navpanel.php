<!-- 
Author: David Onken

IMPORTANT: variablechecker.php should be required before this file.

Contains the items necessary for the nav panel.

Credit for used icons (correct/fail) goes to http://www.famfamfam.com/lab/icons/mini/
-->

<hr/>
	
<table class=navigation id=mainnav>
	<tr>
		<td class=navitem><a href="index.php">Contact Information</a></td>
		<td class=navitem><a href="jobdescription.php">Job Description</a></td>
		<td class=navitem><a href="employment.php">Employment</a></td>
		<td class=navitem><a href="resume.php">Generate Resume</a></td>
		<?php
		redirectToHTTPS();
		
		if (isset($_SESSION['login']))
		{
			echo '<td class=navitem><a href="resumes.php">Load and Save Resumes</a></td>';
			if (getRights($_SESSION['login']) == "admin")
			{
				echo '<td class=navitem><a href="admin.php">User Admin Page</a></td>';
			}
		}?>
	</tr>
	<tr>
		<td class=navitem><?php
		if (contactcheck())
			echo $checkimg;
		else
			echo $failimg; ?></td>
		<td class=navitem><?php
		if (descriptioncheck())
			echo $checkimg;
		else
			echo $failimg; ?></td>
		<td class=navitem><?php
		if (employmentcheck())
			echo $checkimg;
		else
			echo $failimg; ?></td>
		<td class=navitem><?php
		if (employmentcheck() && descriptioncheck() && contactcheck())
			echo $checkimgf;
		else
			echo $failimgf; ?></td>
		<?php
		if (isset($_SESSION['login'])) {
			echo '<td class=navitem>';
			echo $folderimg;
			echo '</td>';
			if (getRights($_SESSION['login']) == "admin"){
				echo '<td class=navitem>';
				echo '<img src="./images/admin.gif" alt="Admin"/>';
				echo '</td>';
			}
		}?>
	</tr>
</table>

<hr/>