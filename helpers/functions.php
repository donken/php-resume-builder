<?php

/*
 * Author: David Onken
 *
 * Useful functions for the db assignment
 */
require("database.php");

// Build table rows from the database's session table
function buildResumesTable($user) {
	
	$resumes = getResumes($user); // Get the needed items from the DBH
	$checked = "checked";
	
	// This builds each individual row. There's a radio button, a name, a date/time, and a dropdown selector.
	foreach ($resumes as $key => $id)
	{
		if (!isset($_SESSION['session_resume'])) // If there is not a resume name selected, use the standard layout (select top)
		{
			echo '<tr><td><input type="radio" class="radioresumeitem" name="resumeitem" value="'.$key.'" '.$checked.'/></td>
			<td>'.$id.'</td>
			<td>'.$key.'</td></tr>';
			$checked = ''; // Checks the first radio button but no others
		}
		else // If the resume name is the same as the current row, check it. Otherwise, don't check it
			if ($key == $_SESSION['session_resume'])
				echo '<tr><td><input type="radio" class="radioresumeitem" name="resumeitem" value="'.$key.'" checked/></td>
				<td>'.$id.'</td>
				<td>'.$key.'</td></tr>';
			else
				echo '<tr><td><input type="radio" class="radioresumeitem" name="resumeitem" value="'.$key.'"/></td>
				<td>'.$id.'</td>
				<td>'.$key.'</td></tr>';
	}
}

// Throws an exception iff the user isn't an admin somehow
function buildUserTable($user) {
	$users = getUsers($user); // Get users
	
	// Build the table
	foreach ($users as $key => $id)
	{
		echo '<tr><td>'.$id->username.'</td><td>'.$id->realname.'</td><td>'.$id->priv.'</td>';
		echo '<td><select name="useroptions[]" id="useroption'.$key.'"><option value="nothing'.$key.'" selected>Nothing</option>';
		if ($id->priv == "admin")
			echo '<option value="ctouser'.$key.'">Change to User</option>';
		else
			echo '<option value="toadmin'.$key.'">Change to Admin</option>';
		echo '<option value="deleteu'.$key.'">Delete User</option></select></td>';
	}
}

/**
 * Builds a resume object for the resumes.php loading process
 * @param unknown_type $resume_name
 * @return resume
 */
function buildResume($resume_name, $user) {
	$resumeobj = new resume;
	$resumeobj->resume_name = $resume_name;
	$resumeobj->setContact(getContactDBH($resume_name, $user)); // Get the contact details
	$resumeobj->setJob(getJobDBH($resume_name, $user)); // Get the job details
	$resumeobj->setEmploy(getEmployDBH($resume_name, $user)); // Get the employment details
	return $resumeobj;
}

/**
 * Since I store the phone weirdly in the database, it needs to be rebuilt properly
 * @param unknown_type $phone
 * @return multitype:NULL
 */
function makePhone($phone) {
	$composedphone = $phone;
	$phonearr = array('phone1' => substr($composedphone, 0, 3),
			'phone2' => substr($composedphone, 4, 3),
			'phone3' => substr($composedphone, -4),);

	return $phonearr;
}