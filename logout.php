<?php
session_start();

$_SESSION['session_resume'] = null;
$_SESSION['login'] = null;

if ($_SERVER['HTTP_REFERER'] == "https://localhost/cs4540/ps5/resumes.php"
		|| $_SERVER['HTTP_REFERER'] == "https://localhost/cs4540/ps5/admin.php")
{
	header('Location: https://localhost/cs4540/ps5/index.php');
}
else
{
	header('Location: '.$_SERVER['HTTP_REFERER']);
}