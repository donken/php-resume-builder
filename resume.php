
<?php 
if(isset($_GET['name']) && isset($_GET['login']))
{
	$name = $_GET['name'];
	$login = $_GET['login'];
	require("helpers/quickload.php");
	exit(0);
} ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Author: David Onken

Preview page to review data.

-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"
	type="text/javascript"></script>
<script src="validate.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="style.css" />
<title>Homework 5</title>
</head>

<body>

	<h2>Homework 5</h2>

<?php

session_start();

if (isset($_SESSION['session_resume']))
	echo "<h3>Resume Name: ".$_SESSION['session_resume']."</h3>";

require ('helpers/variablechecker.php');
require('helpers/loginbox.php');
require('helpers/database.php');
require("helpers/navpanel.php");

/**
 * The previous build table had a remove button functionality which made no sense in this context.
 */
function build_table2()
{
	$descrip = $_SESSION['session_employment'];
	$starts = $_SESSION['session_startdate'];
	$ends = $_SESSION['session_enddate'];

	// Foreach loop
	foreach($descrip as $a => $b)
	{
		echo '<tr><td>'.$descrip[$a].'</td>
		<td>'.$starts[$a].'</td>
		<td>'.$ends[$a].'</td>';
	}

}
?>

<div class=data>

<form method="post" action="resumefinal.php">


	<h3>Review your input data:</h3>

		<table class=information>
			<tr>
				<td>Full Name:</td>
				<td>
					<?php
					
					if ($_SESSION ["session_name"] != null)
						echo $_SESSION ["session_name"];
					?></td>
			</tr>
			<tr>
				<td>Address:</td>
				<td>
					<?php
					// Check for address value
					if ($_SESSION ["session_address"] != null)
						echo $_SESSION ["session_address"];
					?></td>
			</tr>
			<tr>
				<td><label for=phone>Phone Number:</label></td>
				<td id=phone>
					<?php if ($_SESSION["session_phone"] != null) echo $_SESSION["session_phone"]['phone1']; ?>
					-
					<?php if ($_SESSION["session_phone"] != null) echo $_SESSION["session_phone"]['phone2']; ?>
					-
					<?php if ($_SESSION["session_phone"] != null) echo $_SESSION["session_phone"]['phone3']; ?>
					</td>
		
		</table>

		<p id="job_description_label">
			Description of Job you are seeking:
			</p>
			<p>
			<?php if ($_SESSION['session_description'] != null) echo $_SESSION['session_description']; ?>
		</p>

		<table class=information id=jobs>
			<tr>
				<th>Job Description</th>
				<th>Start Date (MM/DD/YYYY)</th>
				<th>End Date (MM/DD/YYYY)</th>
			</tr>
				<?php
				// If we have incoming session data, build it based on that, otherwise make a standard table
				
				if (isset ( $_SESSION ['session_employment'], $_SESSION ['session_startdate'], $_SESSION ['session_enddate'] ))
					build_table2();
				else
					echo '<tr><td>No Jobs Input</td>
	<td>__/__/____</td>
	<td>__/__/____</td></tr>';
				?>

			</table>
			
			<p>Does this look correct? Press submit to generate the resume. <input type="submit" value="Submit" /></p>
			<p id=submittext>
			<?php 
			if (employmentcheck() && descriptioncheck() && contactcheck())
				echo '<p style="color:green">Everything looks good. You may generate your resume!.</p>';
			else
				echo '<p style="color:red">There is something wrong with your input. Return to the page marked in the navigation panel and fix it.</p>'; ?>
				
		</p>
			
			
			</form>

	</div>
	
	<?php require('helpers/footer.php'); ?>

</body>

</html>