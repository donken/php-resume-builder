<?php
session_start();

require('helpers/database.php');
$registerflag = false;
$lengthflag = false;

// If referred here by another page, save it
if ('https://atr.eng.utah.edu/~onken/cs4540/ps5/register.php' != $_SERVER['HTTP_REFERER'])
	$_SESSION['backurl'] = $_SERVER['HTTP_REFERER'];

if (isset($_POST['submit']))
{
	switch ($_POST['submit'])
	{
		case 'Cancel': // If canceled, unset the backspace url and redirect
			$temp = $_SESSION['backurl'];
			unset($_SESSION['backurl']);
			if ($temp == '') // Special case. If they came here directly, send them to index
				$temp = 'https://atr.eng.utah.edu/~onken/cs4540/ps5/index.php';
			header('Location: '.$temp);
			exit(0);
			break;
		case 'Submit':
			if (strlen($_POST['password']) < 8) // Check the length on this end
			{
				$lengthflag = true;
			} else
			{
				//echo "Register ".$_POST['username'];
				$registerflag = inputUser($_POST['username'], $_POST['realname'], $_POST['password']); // Input the user
			}
			break;
	}
}

?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"
	type="text/javascript"></script>
<script src="validate.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="style.css" />
<title>Homework 5</title>
</head>

<body>

	<h2>Homework 5</h2>

<?php

require ('helpers/variablechecker.php');
require("helpers/navpanel.php");

?>
<div class=register><div id=errors>
	<?php 
	
	if (isset($_POST['submit'])) // Error conditions
	{
		if ($registerflag)
			echo "<p style='color:green'>You've successfully registered!<p>";
		else
			echo "<p style='color:red'>That username already exists. Try another one.</p>";
	}
	
	?>
	</div>
<form method="post" onsubmit="return validateRegisterForm()"><table class=information>
				<tr>
					<td><label for=username>Username:</label></td>
					<td><input type="text" id=username name="username"
					<?php
					if (isset($_REQUEST['username']))
						echo 'value="'.$_REQUEST['username'].'" '; ?>/></td>
				</tr>
				<tr>
					<td><label for=realname>Real Name:</label></td>
					<td><input type="text" id=realname name="realname" <?php
					if (isset($_REQUEST['realname']))
						echo 'value="'.$_REQUEST['realname'].'" '; ?>/></td>
				<tr>
					<td><label for=password>Password:</label></td>
					<td><input type="password" id=password name="password" /></td>
				</tr>
				<tr>
					<td><label for=password2>Confirm Password:</label></td>
					<td><input type="password" id=password2 name="password2" /></td>
				</tr>
	</table><input type="submit" name="submit" value="Submit" />
	</form>
	<form method="post">
	<input type="submit" name="submit" value="Cancel" />
	</form>
</div>
<?php require('helpers/footer.php');?>
</body>
</html>