<?php session_start();

require("helpers/variablechecker.php");

// Protection against POST spoofs
if (!checkall())
{
	header('Location: resume.php'); // Direct them back to the resume.php page
	exit();
}


/**
 * Generates li entries based on the session variables
 */
function generateitems() {
	$descrip = $_SESSION['session_employment'];
	$starts = $_SESSION['session_startdate'];
	$ends = $_SESSION['session_enddate'];
	
	// Quick fix
	foreach($descrip as $a => $b)
	{
		if ($descrip[$a] == '' && $starts[$a] == '' && $ends[$a] == '')
		{
			echo '<p>No Work Experience</p>';
			return;
		}
		break;
	}
	
	echo '<ul>';
	foreach($descrip as $a => $b)
	{
		echo '<li class=resumetext>From '.$starts[$a].' to '.$ends[$a].':<br/><br/>' .$descrip[$a].'</li><br/>';
	}
	echo '</ul>';
	
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--

Author: David Onken

Preview page to review data.

-->

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"
	type="text/javascript"></script>
<script src="validate.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="style.css" />
<title>Resume</title>
</head>

<body>

	<p>
		<a class="links" href="resume.php">Go back...</a>
	</p>
	<?php

	require("helpers/database.php");

	?>
	<p id=contactinfo>
		<?php echo $_SESSION["session_name"].'<br/>';
		echo $_SESSION["session_address"].'<br/>';
		echo $_SESSION["session_phone"]["phone1"].'-'.$_SESSION["session_phone"]["phone2"].'-'.$_SESSION["session_phone"]["phone3"];
		 ?>
	</p>

	<hr />
	
	<p class=resumeheader>Jobs that I'm Looking For:</p>
	<p class=resumetext><?php echo $_SESSION["session_description"]; ?></p>
	<hr/>
	
	<p class=resumeheader>Employment Experience:</p>
	<?php generateitems(); ?>
	

</body>

</html>

